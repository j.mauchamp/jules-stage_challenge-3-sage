﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Objets100cLib;

namespace Read_Data_Sage
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //on se connect a la base Bijou a l'aide de la fonction créer 
                var gescomDb = OpenDB(@"C:\Users\Public\Documents\Sage\Entreprise 100c\Bijou.gcm", "<Administrateur>", "");
                Console.WriteLine("bienvenue sur la base commercia {0}", gescomDb.Name);

                //ici on initialise l'article que l'on veut rechercher dans une variable product.
                var product = gescomDb.FactoryArticle.ReadReference("BAAR01");
                Console.WriteLine("Code article : " + product.AR_Ref);
                Console.WriteLine("Intitulé article : " + product.AR_Design);
                Console.WriteLine("Intitulé famille d'articles : " + product.Famille.FA_Intitule);
                Console.WriteLine("Prix d'achat : " + product.AR_PrixAchat);
                Console.WriteLine("prix de vente : " + product.AR_PrixVen);

                Console.WriteLine("----------------------------------");

                //on initialise le document de vente que l'on veut rechercher.
                var docVente = gescomDb.FactoryDocumentVente.ReadPiece(DocumentType.DocumentTypeVenteCommande, "BC00014");
                Console.WriteLine("Numéro de document : " + docVente.DO_Piece);
                Console.WriteLine("Référence de document : " + docVente.DO_Ref);
                Console.WriteLine("Date de document : " + docVente.DO_Date);
                Console.WriteLine("Status du document : " + docVente.DO_Statut);
                Console.WriteLine("Code client associé au document : " + docVente.Client.CT_Num);
                Console.WriteLine("Intitulé client associé au document : " + docVente.Client.CT_Intitule);
                Console.WriteLine("Nom du représentant assocé au document : " + docVente.Collaborateur.Nom);
                Console.WriteLine("Prénom du représentant assocé au document : " + docVente.Collaborateur.Prenom);
                Console.WriteLine("Adresse de livraison associée au document : " + docVente.LieuLivraison.LI_Intitule + " " + docVente.LieuLivraison.Adresse.Adresse + " " + docVente.LieuLivraison.Adresse.Ville + " " + docVente.LieuLivraison.Adresse.CodePostal);

                Console.WriteLine("----------------------------------");

                //on initialise une variable qui va prendre les familles
                var familleListe = gescomDb.FactoryFamille.List;
                
                foreach (IBOFamille3 famille in familleListe)
                {
                    if (famille.FA_Type == FamilleType.FamilleTypeDetail)
                    {
                        Console.WriteLine("Code famille : " + famille.FA_CodeFamille);
                        Console.WriteLine("Intitulé de la famille : " + famille.FA_Intitule);
                    }

                }

                Console.WriteLine("----------------------------------");

                //on initialise une variable qui va prendre les modes d'expeditions
                var modeLivraison = gescomDb.FactoryExpedition.List;

                foreach (IBPExpedition3 mode in modeLivraison)
                {
                    if (!string.IsNullOrEmpty(mode.E_Intitule))
                    {
                        Console.WriteLine("Code mode d'éxpedition : " + mode.E_Mode);
                        Console.WriteLine("Intitulé mode d'éxpedition : " + mode.E_Intitule);
                    }
                }

                Console.Read();
            }
            catch
            {
                Console.WriteLine("erreur a la connexion de la base");
            }
        }

        //création d'une fonction pour ouvrir la base.
        public static BSCIALApplication100c OpenDB(string filepath, string user, string password)
        {
            var BaseCom = new BSCIALApplication100c();

            BaseCom.Name = filepath;
            BaseCom.Loggable.UserName = user;
            BaseCom.Loggable.UserPwd = password;
            BaseCom.Open();

            return BaseCom;

        }

    }
}
